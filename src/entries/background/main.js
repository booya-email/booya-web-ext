import browser from "webextension-polyfill"

function buildContextMenu() {
    console.log("Building menus");
    browser.contextMenus.removeAll().then(() => {
        browser.storage.local.get("settings", store => {
            if (store.settings) {
                var domains = store.settings.domain_names.split(',');
                for (var i = 0; i < domains.length; i++) {
                    browser.contextMenus.create({
                        id: domains[i],
                        title: domains[i],
                        contexts: ["editable"]
                    });
                }
            }
        });
    });
}

browser.runtime.onInstalled.addListener(() => {
    console.log("Extension installed");
    buildContextMenu();
});

browser.runtime.onMessage.addListener(request => {
    if (request.msg === "build-context-menu") {
        buildContextMenu();
    }

    if (request.msg === "create-notification") {
        browser.notifications.create({
            "type": "basic",
            "iconUrl": browser.extension.getURL("icons/48.png"),
            "title": "Alias successfully created! (copied)",
            "message": request.alias
        });
    }
});

browser.menus.onClicked.addListener((info, tab) => {
    browser.tabs.sendMessage(tab.id, { 
        msg:            "create-alias",
        url:            tab.url,
        domain_name:    info.menuItemId,
        text_field_id:  info.targetElementId
    });
});
