import { createApp } from "vue";
import Popup from "./Popup.vue";
import '../../assets/index.css';

import { library } from '@fortawesome/fontawesome-svg-core'
import { faCopy, faTrash, faXmark, faFloppyDisk, faSquarePlus, faEnvelope, faCircleNotch, faCaretRight, faCircleInfo } from '@fortawesome/free-solid-svg-icons'
library.add(faCopy, faTrash, faXmark, faFloppyDisk, faSquarePlus, faEnvelope, faCircleNotch, faCaretRight, faCircleInfo);
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

createApp(Popup)
    .component("font-awesome-icon", FontAwesomeIcon)
    .mount("#app");
