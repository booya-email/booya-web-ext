import browser from "webextension-polyfill";
import * as Alias from "../../lib/alias"

browser.runtime.onMessage.addListener(request => {
  if (request.msg == "create-alias") {
    const alias = Alias.buildAlias(request.url, request.domain_name);
    const text_field = browser.menus.getTargetElement(request.text_field_id);

    browser.storage.local.get("settings", store => {
      const api_key = store.settings.api_key;
      const destination_email = store.settings.destination_email;

      Alias.createAlias(request.domain_name, api_key, alias, destination_email)
        .then(response => {
          if (response.status == 201) {
            text_field.value = alias;
            navigator.clipboard.writeText(alias);
            browser.runtime.sendMessage({ msg: "create-notification", alias: alias });
          } else {
            text_field.value = response.status + " - " + response.statusText;
          }
        });
    });
  }
});
