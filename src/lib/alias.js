async function createAlias(domain_name, api_key, alias, destination_email) {
    const response = await fetch("https://api.gandi.net/v5/email/forwards/" + domain_name, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Authorization': 'Apikey ' + api_key,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ source: alias, destinations: [destination_email] })
    });
    return response;
}

function getDomainName(url) {
    const regex = /^www\./g;
    var u = new URL(url);
    return u.hostname.replace(regex, '');
}

function generateAlias(length) {
    var result = [];
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;

    for (var i = 0; i < length; i++) {
        result.push(characters.charAt(Math.floor(Math.random() * charactersLength)));
    }
    return result.join('');
}

function buildAlias(url, domain_name) {
    var d = getDomainName(url);
    var a = generateAlias(10);
    return a + "-" + d + '@' + domain_name;
}

async function getAliases(api_key, domain, page = 1, per_page = 20, source = "*") {
    const url = "https://api.gandi.net/v5/email/forwards/" + domain + "?" + "page=" + page + "&per_page=" + per_page + "&source=" + source;
    const response = await fetch(url, {
        method: 'GET',
        mode: 'cors',
        headers: {
            'Authorization': 'Apikey ' + api_key,
        }
    });
    return response;
}

async function deleteAlias(api_key, domain, source) {
    const url = "https://api.gandi.net/v5/email/forwards/" + domain + "/" + source
    const response = await fetch(url, {
        method: 'DELETE',
        mode: 'cors',
        headers: {
            'Authorization': 'Apikey ' + api_key,
        }
    });
    return response;
}


export { createAlias, buildAlias, getAliases, deleteAlias };