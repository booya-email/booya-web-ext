/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{vue,js,ts,jsx,tsx}"],
  safelist: ['bg-bred', 'bg-bemerald'], // Make sure these classes get included (dynamic colors for alert): https://www.youtube.com/watch?v=waoOK5s9ypc&t=315s
  theme: {
    extend: {
      colors: {
        bred: '#F94144',
        borange: '#F8961E',
        byellow: '#F9C74F',
        bgreen: '#90BE6D',
        bemerald: '#43AA8B',
        bblue: '#577590',
        bblack: '#212121'
      }
    }
  },
  plugins: [],
}
