# Booya - Web Extension

This web extension allows you to create forward email address for a Gandi.net managed domain name.

This is useful to hide your real email address from websites.

# Contribute

The extension is developed with Vue 3 and built with Vite.

## Project Setup

Fork this repository, then:

```sh
cd booya-web-ext
npm install
```

## Commands

### Build

#### Development, HMR

Hot Module Reloading is used to load changes inline without requiring extension rebuilds and extension/page reloads
```sh
npm run dev
```

#### Development, Watch

Rebuilds extension on file changes. Requires a reload of the extension (and page reload if using content scripts)
```sh
npm run watch
```

#### Production

Minifies and optimizes extension build
```sh
npm run build
```

### Load Extension in Browser

Loads the contents of the dist directory into the specified browser.

```sh
npm run serve:chrome
```

```sh
npm run serve:firefox
```

## Usage Notes

The extension manifest is defined in `src/manifest.js` and used by `@samrum/vite-plugin-web-extension` in the vite config.

Background, content scripts, options, and popup entry points exist in the `src/entries` directory. 

Content scripts are rendered by `src/entries/contentScript/renderContent.js` which renders content within a ShadowRoot
and handles style injection for HMR and build modes.

See [Vite Configuration Reference](https://vitejs.dev/config/).

Otherwise, the project functions just like a regular Vite project.

Refer to [@samrum/vite-plugin-web-extension](https://github.com/samrum/vite-plugin-web-extension) for more usage notes or to start to build your own extension.