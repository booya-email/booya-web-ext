import pkg from "../package.json";

const manifest = {
  background: {
    scripts: ["src/entries/background/main.js"],
    persistent: false
  },
  content_scripts: [
    {
      js: ["src/entries/contentScript/main.js"],
      matches: ["*://*/*"]
    },
  ],
  browser_action: {
    default_icon: "icons/48.png",
    default_title: "Booya!",
    default_popup: "src/entries/popup/index.html"
  },
  icons: {
    48: "icons/48.png",
    96: "icons/96.png"
  },
  options_ui: {
    chrome_style: false,
    open_in_tab: true,
    page: "src/entries/options/index.html"
  },
  permissions: [
    "*://api.gandi.net/*",
    "activeTab",
    "contextMenus",
    "storage",
    "tabs",
    "clipboardWrite",
    "menus",
    "notifications"
  ]
};

export function getManifest() {
  return {
    author: pkg.author,
    description: pkg.description,
    name: pkg.displayName ?? pkg.name,
    version: pkg.version,
    manifest_version: 2,
    ...manifest,
  };
}
